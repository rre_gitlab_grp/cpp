### 1. **Classes and Objects:**

- **Definition:** A class is a blueprint for creating objects. Objects are instances of a class that encapsulate data (attributes) and behaviors (methods).

- **Example:**

  ```c++
  // Class definition
  class Car {
  public:
      // Attributes
      std::string brand;
      int year;
  
      // Method to display information
      void displayInfo() {
          std::cout << "Brand: " << brand << ", Year: " << year << std::endl;
      }
  };
  
  int main() {
      // Creating objects of the Car class
      Car car1;
      car1.brand = "Toyota";
      car1.year = 2022;
  
      Car car2;
      car2.brand = "Honda";
      car2.year = 2021;
  
      // Accessing object methods
      car1.displayInfo();
      car2.displayInfo();	;		
  
      return 0;
  }
  ```

### 2. **Constructor and Destructor:**

- **Constructor:** A special method called when an object is created. It initializes the object's attributes.

- **Destructor:** A special method called when an object goes out of scope. It performs cleanup tasks.

- **Example:**

  ```c++
  class Person {
  public:
      // Constructor
      Person(std::string n, int a) : name(n), age(a) {
          std::cout << "Constructor called" << std::endl;
      }
  
      // Destructor
      ~Person() {
          std::cout << "Destructor called for " << name << std::endl;
      }
  
      void displayInfo() {
          std::cout << "Name: " << name << ", Age: " << age << std::endl;
      }
  
  private:
      std::string name;
      int age;
  };
  
  int main() {
      Person person1("Alice", 25);
      person1.displayInfo();
  
      Person person2("Bob", 30);
      person2.displayInfo();
  
      // Destructor is automatically called when objects go out of scope
      return 0;
  }
  ```

### 3. **Encapsulation:**

- **Definition:** Encapsulation is the bundling of data (attributes) and methods that operate on the data into a single unit (class). It restricts direct access to some of an object's components.

- **Example:**

  ```c++
  class BankAccount {
  public:
      // Setter for balance
      void setBalance(double amount) {
          if (amount >= 0) {
              balance = amount;
          } else {
              std::cout << "Invalid amount" << std::endl;
          }
      }
  
      // Getter for balance
      double getBalance() const {
          return balance;
      }
  
  private:
      double balance = 0.0;
  };
  
  int main() {
      BankAccount account;
      account.setBalance(1000.50);
  
      // Direct access to balance is restricted
      // double accBalance = account.balance;  // Error
  
      // Access balance using the getter method
      double accBalance = account.getBalance();
      std::cout << "Account Balance: " << accBalance << std::endl;
  
      return 0;
  }
  ```

### 4. **Inheritance:**

- **Definition:** Inheritance is a mechanism that allows a class to inherit properties and behaviors from another class. It promotes code reuse.

- **Example:**

  ```c++
  class Animal {
  public:
      void eat() {
          std::cout << "Animal is eating" << std::endl;
      }
  };
  
  class Dog : public Animal {
  public:
      void bark() {
          std::cout << "Dog is barking" << std::endl;
      }
  };
  
  int main() {
      Dog myDog;
      myDog.eat();  // Inherited from Animal
      myDog.bark(); // From Dog class
  
      return 0;
  }
  ```

### 5. **Polymorphism:**

- **Definition:** Polymorphism allows objects of different types to be treated as objects of a common base type. It is achieved through function overloading and virtual functions.

- **Example:**

  ```c++
  class Shape {
  public:
      virtual void draw() const {
          std::cout << "Drawing a shape" << std::endl;
      }
  };
  
  class Circle : public Shape {
  public:
      void draw() const override {
          std::cout << "Drawing a circle" << std::endl;
      }
  };
  
  class Square : public Shape {
  public:
      void draw() const override {
          std::cout << "Drawing a square" << std::endl;
      }
  };
  
  void drawShape(const Shape& shape) {
      shape.draw();
  }
  
  int main() {
      Circle myCircle;
      Square mySquare;
      drawShape(myCircle);  // Calls draw() from Circle class
      drawShape(mySquare);   // Calls draw() from Square class
  
      return 0;
    }
  ```

### 6. **Abstraction:**

   - **Definition:** Abstraction involves simplifying complex systems by modeling classes based on the essential properties and behaviors they share. Abstract classes and pure virtual functions are used to achieve abstraction.

   - **Example:**

```c++

     // Abstract base class
     class Shape {
     public:
         // Pure virtual function makes the class abstract
         virtual double calculateArea() const = 0;

         // Common method for all shapes
         void printInfo() const {
             std::cout << "This is a shape" << std::endl;
         }
     };

     // Derived class - Circle
     class Circle : public Shape {
     public:
         Circle(double r) : radius(r) {}

         // Implementation of the pure virtual function
         double calculateArea() const override {
             return 3.14 * radius * radius;
         }

     private:
         double radius;
     };

     // Derived class - Square
     class Square : public Shape {
     public:
         Square(double s) : side(s) {}

         // Implementation of the pure virtual function
         double calculateArea() const override {
             return side * side;
         }

     private:
         double side;
     };

     int main() {
         Circle myCircle(5.0);
         Square mySquare(4.0);

         // Using polymorphism through pointers to the base class
         Shape* shape1 = &myCircle;
         Shape* shape2 = &mySquare;

         // Call to the pure virtual function is resolved at runtime
         std::cout << "Area of Circle: " << shape1->calculateArea() << std::endl;
         std::cout << "Area of Square: " << shape2->calculateArea() << std::endl;

         // Common method from the base class
         shape1->printInfo();
         shape2->printInfo();

         return 0;
     }
    


```

These examples cover the core concepts of Object-Oriented Programming in C++. Remember that these concepts are interconnected, and their effective use leads to well-structured, modular, and maintainable code. Continue exploring and practicing these concepts to become proficient in C++ OOP.

### . **Smart Pointers:**

- **Definition:** Smart pointers are objects that act like pointers but provide additional functionality, such as automatic memory management.

- **Example:**

  ```c++
  #include <iostream>
  #include <memory>
  
  class MyClass {
  public:
      MyClass() {
          std::cout << "Constructor called" << std::endl;
      }
  
      ~MyClass() {
          std::cout << "Destructor called" << std::endl;
      }
  
      void showMessage() {
          std::cout << "Hello from MyClass!" << std::endl;
      }
  };
  
  int main() {
      // Using unique_ptr for automatic memory management
      std::unique_ptr<MyClass> myClassPtr = std::make_unique<MyClass>();
  
      // Accessing member function
      myClassPtr->showMessage();
  
      // No need to explicitly delete, as unique_ptr manages memory
      return 0;
  }
  ```

### 2. **Lambda Expressions:**

- **Definition:** Lambda expressions allow the creation of anonymous functions directly within code.

- **Example:**

  ```c++
  #include <iostream>
  
  int main() {
      // Lambda expression to add two numbers
      auto add = [](int a, int b) {
          return a + b;
      };
  
      // Using the lambda function
      std::cout << "Sum: " << add(5, 7) << std::endl;
  
      return 0;
  }
  ```

### 3. **Move Semantics:**

- **Definition:** Move semantics allow the efficient transfer of resources from one object to another without unnecessary copying.

- **Example:**

  ```c++
  #include <iostream>
  #include <vector>
  
  class MyObject {
  public:
      MyObject() {
          std::cout << "Constructor called" << std::endl;
      }
  
      ~MyObject() {
          std::cout << "Destructor called" << std::endl;
      }
  
      // Move constructor
      MyObject(MyObject&& other) noexcept {
          std::cout << "Move constructor called" << std::endl;
          // Move resources from 'other' to 'this'
      }
  };
  
  int main() {
      std::vector<MyObject> vec;
  
      // Using push_back with a temporary object
      vec.push_back(MyObject());  // Move semantics are invoked
  
      return 0;
  }
  ```

### 4. **RAII (Resource Acquisition Is Initialization):**

- **Definition:** RAII is a C++ programming paradigm where resource management is tied to object lifetime.

- **Example:**

  ```c++
  #include <iostream>
  #include <fstream>
  
  class FileHandler {
  public:
      FileHandler(const std::string& filename) : file(filename) {
          std::cout << "File opened: " << filename << std::endl;
      }
  
      ~FileHandler() {
          file.close();
          std::cout << "File closed" << std::endl;
      }
  
      // Other methods for file manipulation
  
  private:
      std::ifstream file;
  };
  
  int main() {
      FileHandler fileHandler("example.txt");
  
      // FileHandler destructor is automatically called when 'fileHandler' goes out of scope
      return 0;
  }
  ```

### 5. **Concurrency with Threads:**

- **Definition:** C++ provides the `<thread>` library for working with multithreading.

- **Example:**

  ```c++
  #include <iostream>
  #include <thread>
  
  // Function to be executed in a separate thread
  void threadFunction() {
      for (int i = 0; i < 5; ++i) {
          std::cout << "Thread: " << i << std::endl;
      }
  }
  
  int main() {
      // Create a thread and launch the function
      std::thread myThread(threadFunction);
  
      // Do some work in the main thread
      for (int i = 0; i < 3; ++i) {
          std::cout << "Main: " << i << std::endl;
      }
  
      // Wait for the thread to finish
      myThread.join();
  
      return 0;
  }
  ```

### 6. **Variadic Templates:**

- **Definition:** Variadic templates allow the creation of functions and classes that accept a variable number of arguments.

- **Example:**

  ```c++
  #include <iostream>
  
  // Variadic template function to print multiple values
  template<typename T>
  void printValues(const T& value) {
      std::cout << value << std::endl;
  }
  
  template<typename T, typename... Args>
  void printValues(const T& value, const Args&... args) {
      std::cout << value << ", ";
      printValues(args...);
  }
  
  int main() {
      printValues(1, 2.5, "Hello", 'A');
  
      return 0;
  }
  ```

These advanced concepts showcase the power and flexibility of C++. Each concept addresses specific programming challenges and contributes to writing efficient, maintainable, and expressive code. Continuously exploring and applying these concepts will enhance your C++ programming skills.



### 7. **Concurrency and Parallelism with `std::async` and `std::future`:**

- **Definition:** `std::async` and `std::future` provide a convenient way to perform asynchronous or parallel execution of functions.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  #include <future>
  
  int add(int a, int b) {
      return a + b;
  }
  
  int main() {
      // Asynchronous execution of the 'add' function
      std::future<int> result = std::async(add, 10, 20);
  
      // Do other work in the main thread
  
      // Get the result from the async operation
      int sum = result.get();
  
      std::cout << "Sum: " << sum << std::endl;
  
      return 0;
  }
  ```

### 8. **Concurrency with `std::mutex` and `std::lock_guard`:**

- **Definition:** `std::mutex` provides a mechanism for mutual exclusion to protect shared data in a multithreaded environment.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  #include <thread>
  #include <mutex>
  
  std::mutex myMutex;
  
  void sharedResourceFunction(int id) {
      std::lock_guard<std::mutex> lock(myMutex);
      std::cout << "Thread " << id << " accessing shared resource." << std::endl;
      // Critical section
  }
  
  int main() {
      const int numThreads = 3;
      std::thread threads[numThreads];
  
      // Launch threads to access shared resource
      for (int i = 0; i < numThreads; ++i) {
          threads[i] = std::thread(sharedResourceFunction, i);
      }
  
      // Join threads to the main thread
      for (int i = 0; i < numThreads; ++i) {
          threads[i].join();
      }
  
      return 0;
  }
  ```

### 9. **Type Traits and SFINAE (Substitution Failure Is Not An Error):**

- **Definition:** Type traits provide a way to query and transform types at compile-time. SFINAE is a technique that leverages template substitution to conditionally enable or disable function overloads.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  #include <type_traits>
  
  template <typename T>
  void printIfInteger(const T& value) {
      if (std::is_integral<T>::value) {
          std::cout << "Integer value: " << value << std::endl;
      } else {
          std::cout << "Not an integer" << std::endl;
      }
  }
  
  int main() {
      printIfInteger(42);      // Output: Integer value: 42
      printIfInteger("hello"); // Output: Not an integer
  
      return 0;
  }
  ```

### 10. **constexpr and Compile-Time Programming:**

- **Definition:** `constexpr` allows computations to be performed at compile-time, enabling more efficient and flexible code.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  
  constexpr int factorial(int n) {
      return (n <= 1) ? 1 : n * factorial(n - 1);
  }
  
  int main() {
      constexpr int result = factorial(5);
      std::cout << "Factorial of 5: " << result << std::endl;
  
      return 0;
  }
  ```

### 11. **Template Metaprogramming (TMP):**

- **Definition:** TMP is a C++ technique where templates are used to perform computations at compile-time.

- **Example:**

  ```c++
  #include <iostream>
  
  template <int N>
  struct Factorial {
      static const int value = N * Factorial<N - 1>::value;
  };
  
  template <>
  struct Factorial<0> {
      static const int value = 1;
  };
  
  int main() {
      constexpr int result = Factorial<5>::value;
      std::cout << "Factorial of 5: " << result << std::endl;
  
      return 0;
  }
  ```

These advanced concepts showcase the versatility and power of C++ in handling complex scenarios. Each concept addresses specific challenges and enables more efficient, flexible, and expressive programming. Continue exploring these concepts and applying them in your projects to deepen your understanding of advanced C++ programming.



### 12. **Custom Memory Management with `new` and `delete`:**

- **Definition:** While smart pointers (e.g., `std::unique_ptr`, `std::shared_ptr`) provide automatic memory management, understanding manual memory allocation and deallocation is crucial for advanced C++ programming.

- **Example:**

  ```c++
  #include <iostream>
  
  class CustomObject {
  public:
      CustomObject() {
          std::cout << "Constructor called" << std::endl;
      }
  
      ~CustomObject() {
          std::cout << "Destructor called" << std::endl;
      }
  };
  
  int main() {
      // Manual memory allocation
      CustomObject* obj = new CustomObject();
  
      // Do work with obj
  
      // Manual memory deallocation
      delete obj;
  
      return 0;
  }
  ```

### 13. **C++ Standard Template Library (STL) Algorithms:**

- **Definition:** The STL provides a rich set of algorithms that operate on containers, making it easier to perform common operations.

- **Example:**

  ```c++
  #include <iostream>
  #include <vector>
  #include <algorithm>
  
  int main() {
      std::vector<int> numbers = {5, 2, 8, 1, 3};
  
      // Using STL algorithms
      std::sort(numbers.begin(), numbers.end());
  
      // Print sorted numbers
      for (const auto& num : numbers) {
          std::cout << num << " ";
      }
      std::cout << std::endl;
  
      return 0;
  }
  ```

### 14. **C++17 Features:**

- **Structured Bindings:**

  ```c++
  #include <iostream>
  #include <tuple>
  
  int main() {
      std::tuple<int, double, std::string> myTuple = {42, 3.14, "Hello"};
  
      // Using structured bindings
      auto [num, value, message] = myTuple;
  
      std::cout << "Tuple Values: " << num << ", " << value << ", " << message << std::endl;
  
      return 0;
  }
  ```

- **Filesystem Library:**

  ```c++
  #include <iostream>
  #include <filesystem>
  
  int main() {
      // C++17 filesystem library
      std::filesystem::path currentPath = std::filesystem::current_path();
  
      std::cout << "Current Path: " << currentPath << std::endl;
  
      return 0;
  }
  ```

```c++
#include <iostream>
using namespace std;

// Template class for a Pair
template <typename T1, typename T2>
class Pair {
private:
    T1 first;
    T2 second;

public:
    // Constructor to initialize the pair
    Pair(const T1& f, const T2& s) : first(f), second(s) {}

    // Getter functions
    T1 getFirst() const {
        return first;
    }

    T2 getSecond() const {
        return second;
    }

    // Display the pair
    void display() const {
        cout << "Pair: (" << first << ", " << second << ")" << endl;
    }
};

int main() {
    // Create pairs of different types
    Pair<int, double> intDoublePair(5, 3.14);
    Pair<string, char> stringCharPair("Hello", 'A');

    // Display the pairs
    intDoublePair.display();
    stringCharPair.display();

    // Access individual elements
    cout << "First element of intDoublePair: " << intDoublePair.getFirst() << endl;
    cout << "Second element of stringCharPair: " << stringCharPair.getSecond() << endl;

    return 0;
}
```

### 15. **User-Defined Literals:**

- **Definition:** User-defined literals allow developers to create custom suffixes for numeric and string literals.

- **Example:**

  ```c++
  #include <iostream>
  
  // User-defined literal for converting degrees to radians
  constexpr double operator "" _deg(long double deg) {
      return deg * 3.141592 / 180.0;
  }
  
  int main() {
      // Using the user-defined literal
      double radians = 90.0_deg;
  
      std::cout << "90 degrees in radians: " << radians << std::endl;
  
      return 0;
  }
  ```

These advanced concepts showcase additional features and capabilities of C++ that go beyond the basics. They are essential for writing efficient, expressive, and modern C++ code. Continue exploring and experimenting with these concepts to enhance your proficiency in advanced C++ programming.



### 16. **Template Specialization:**

- **Definition:** Template specialization allows providing a different implementation for a template for specific argument types.

- **Example:**

  ```c++
  #include <iostream>
  
  // Primary template
  template <typename T>
  struct Printer {
      static void print(const T& value) {
          std::cout << "Generic: " << value << std::endl;
      }
  };
  
  // Template specialization for strings
  template <>
  struct Printer<std::string> {
      static void print(const std::string& value) {
          std::cout << "String: " << value << std::endl;
      }
  };
  
  int main() {
      Printer<int>::print(42);            // Calls the generic version
      Printer<std::string>::print("Hi"); // Calls the specialized version
  
      return 0;
  }
  ```

### 17. **Template Metaprogramming (TMP) with `std::enable_if`:**

- **Definition:** `std::enable_if` is a mechanism for conditionally enabling or disabling function overloads based on type traits.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  #include <type_traits>
  
  template <typename T>
  typename std::enable_if<std::is_integral<T>::value, T>::type
  addOne(T value) {
      return value + 1;
  }
  
  template <typename T>
  typename std::enable_if<!std::is_integral<T>::value, T>::type
  addOne(T value) {
      return value;
  }
  
  int main() {
      std::cout << "Result (int): " << addOne(5) << std::endl;        // Calls the first version
      std::cout << "Result (double): " << addOne(3.14) << std::endl;  // Calls the second version
  
      return 0;
  }
  ```

### 18. **Template Template Parameters:**

- **Definition:** Template template parameters allow templates to accept other templates as arguments.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  
  // Template template parameter
  template <template <typename> class Container, typename T>
  void printContainer(const Container<T>& container) {
      for (const auto& element : container) {
          std::cout << element << " ";
      }
      std::cout << std::endl;
  }
  
  int main() {
      std::vector<int> myVector = {1, 2, 3, 4, 5};
      std::list<double> myList = {2.5, 4.7, 6.1};
  
      printContainer(myVector); // Prints elements of vector
      printContainer(myList);   // Prints elements of list
  
      return 0;
  }
  ```

### 19. **Variadic Templates with Parameter Packs:**

- **Definition:** Variadic templates with parameter packs allow functions and classes to accept a variable number of template arguments.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  
  // Variadic template function
  template <typename... Args>
  void printValues(Args... args) {
      ((std::cout << args << " "), ...);
      std::cout << std::endl;
  }
  
  int main() {
      printValues(1, 2.5, "Hello", 'A');
  
      return 0;
  }
  ```

### 20. **CRTP (Curiously Recurring Template Pattern):**

- **Definition:** CRTP is a design pattern where a class template inherits from its derived class, enabling static polymorphism.

- **Example:**

  ```c++
  cppCopy code#include <iostream>
  
  // CRTP base class
  template <typename Derived>
  struct Base {
      void doSomething() {
          static_cast<Derived*>(this)->implementation();
      }
  };
  
  // Derived class using CRTP
  struct Derived : public Base<Derived> {
      void implementation() {
          std::cout << "Doing something specific in the derived class" << std::endl;
      }
  };
  
  int main() {
      Derived obj;
      obj.doSomething(); // Calls the implementation in the derived class
  
      return 0;
  }
  ```

These advanced template concepts showcase the flexibility and power of template metaprogramming in C++. Templates are a versatile feature, and mastering them allows for writing highly generic and efficient code. Explore these concepts in your projects to deepen your understanding of template programming in C++.