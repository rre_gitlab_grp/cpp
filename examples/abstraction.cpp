#include "iostream"
using namespace std;
class Shape{
private:
public:
    virtual double calculateArea() const = 0;
    //    common method for all shapes
    void printInfo() const
    {
        cout << "This is a shape method" << endl;
    }

    // Virtual destructor
    virtual ~Shape() {
        cout << "This is a shape virtual distructor" << endl;
    }
};

class Circle : public Shape{
private:
    double radius;
public:
    Circle(double r) : radius(r){

    }
    double calculateArea() const override
    {
        return (3.14 * radius *radius);
    }
};

class Square : public Shape{
private :
        double side;
public:
    Square(double s) : side(s){}

    double calculateArea() const override
    {
        return (side * side);
    }

};

int main ()
{
    double result;

    Circle myCircle(6);
    Square mySquare(9.0);
    result = myCircle.calculateArea();
    cout << "mycircle area : " << result;

    cout << "\nsquare area :" << mySquare.calculateArea() << endl;

    Shape *basePtr;
    basePtr = &myCircle;
    cout << "basePtr wit circle " << basePtr->calculateArea() <<endl;

    basePtr = &mySquare;
    cout << "basePtr wit square " << basePtr->calculateArea() <<endl;


}