#include "iostream"
using namespace std;

class BankAccount{
private :
    double balance = 0.0;
public:
    void setBalance(double amount)
    {
        if(amount >=0)
        {
            balance = amount;
        }
        else{
            cout << "invali amount" << endl;
        }

    }
    double getBalance(){
        return balance;
    }
};

int main(){
    BankAccount account;
    account.setBalance(100);
    cout << account.getBalance();
}