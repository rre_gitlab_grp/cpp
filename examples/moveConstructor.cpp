#include <iostream>
#include <vector>

class MyObject {
public:
    MyObject() {
        std::cout << "Constructor called" << std::endl;
    }

    ~MyObject() {
        std::cout << "Destructor called" << std::endl;
    }

    void displayInfo(){
        std::cout << "print" << std::endl;
    }

    // Move constructor
    MyObject(MyObject&& other) noexcept {
        std::cout << "Move constructor called" << std::endl;
        // Move resources from 'other' to 'this'
    }
};

int main() {
    std::vector<MyObject> vec;

    // Using push_back with a temporary object
    vec.push_back(MyObject());  // Move semantics are invoked

    vec[0].displayInfo();

    return 0;
}