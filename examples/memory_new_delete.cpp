#include "iostream"
using namespace std;
class NewClass{
private:
public:
    NewClass()
    {
    cout << "Constructor is called" << endl;
    }
    ~NewClass()
    {
    cout << "Distructor is called" << endl;
    }
};

int main ()
{
    NewClass* clsPtr;
    clsPtr = new NewClass();
    delete clsPtr;

    return 0;
}