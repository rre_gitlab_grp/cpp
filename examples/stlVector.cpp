#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// Function to print the elements of a vector
void printVector(const vector<int>& vec) {
    for( auto num : vec) {
        cout << num << " ";
    }
    cout << endl;
}

int main() {
    vector<int> numbers = {9, 2, 7, 4, 5, 8};

    // Sorting the vector
    sort(numbers.begin(), numbers.end());

    // Calling the printVector function to print the sorted elements
    printVector(numbers);

    return 0;
}
