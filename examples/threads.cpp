#include <iostream>
#include <thread>

// Function to be executed in a separate thread
void threadFunction() {
    for (int i = 0; i < 15; ++i) {
        std::cout << "\nThread: " << i << std::endl;
    }
}

int main() {
    // Create a thread and launch the function
    std::thread myThread(threadFunction);

    // Do some work in the main thread
    for (int i = 0; i < 23; ++i) {
        std::cout << "\nMain: " << i << std::endl;
    }

    // Wait for the thread to finish
    myThread.join();

    return 0;
}