#include "iostream"
using namespace std;
class Shape{
private:
public:
    virtual void draw() const
    {
        cout << "drwaing a shape" << endl;

    }
};

class Circle : public Shape{
public:
    void draw() const override
    {
        cout << "Drawing a Circle" << endl;
    }
};
class Square : public Shape{
public:
    void draw() const override
    {
        cout << "Drawing Square" << endl;
    }

};

int main()
{
    Shape myShape;
    Circle myCircle;
    Square mySquare;
    myShape.draw();
    myCircle.draw();
    mySquare.draw();

    return 0;
}