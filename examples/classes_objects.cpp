#include "iostream"
using namespace std;

class Car{
public :
    string brand;
    int year;

    void displayInfo()
    {
        cout << "brand :" << brand <<", year :" << year << endl;
    }

};

int main ()
{
    Car car1;
    car1.brand = "rre";
    car1.year =2023;

    Car car2;
    car2.brand = "Divya";
    car2.year = 1988;

    car1.displayInfo();
    car2.displayInfo();

    return 0;
}