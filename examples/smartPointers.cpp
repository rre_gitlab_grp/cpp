#include "iostream"
#include "memory"
using namespace std;

class MyClass {
public:
    MyClass() {
        cout << "Constructor called" << endl;
    }

    ~MyClass() {
        cout << "Destructor called" << endl;
    }

    void showMessage() {
        cout << "Hello from MyClass!" << endl;
    }
};

int main() {
    // Using unique_ptr for automatic memory management
    unique_ptr<MyClass> myClassPtr = make_unique<MyClass>();

    // Accessing member function
    myClassPtr->showMessage();

    // No need to explicitly delete, as unique_ptr manages memory
    return 0;
}