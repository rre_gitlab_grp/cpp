#include <iostream>
using namespace std;

// Template class for a Pair
template <typename T1, typename T2>
class CustomeStl {
private:
    T1 first;
    T2 second;

public:
    // Constructor to initialize the pair
    CustomeStl(const T1& f, const T2& s) : first(f), second(s) {}

    // Getter functions
    T1 getFirst() const {
        return first;
    }

    T2 getSecond() const {
        return second;
    }

    // Display the pair
    void display() const {
        cout << "Pair: (" << first << ", " << second << ")" << endl;
    }
};

int main() {
    // Create pairs of different types
    CustomeStl<int, double> intDoublePair(5, 3.14);
    CustomeStl<string, char> stringCharPair("Hello", 'A');

    // Display the pairs
    intDoublePair.display();
    stringCharPair.display();

    // Access individual elements
    cout << "First element of intDoublePair: " << intDoublePair.getFirst() << endl;
    cout << "Second element of stringCharPair: " << stringCharPair.getSecond() << endl;

    return 0;
}