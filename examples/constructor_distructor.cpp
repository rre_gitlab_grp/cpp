#include "iostream"
using namespace std;

class Person{
public:
    Person(string n, int a) : name(n), age(a){
        cout << "Constructor is called" << endl;
    }
    ~Person (){
        cout << "distructor is called" << endl;
    };
    void displayInfo(){
        cout << "name :" << name << ", age :"  << age << endl;
    }

private:
    string name;
    int age;
};

int main (){
    Person person1("Reshwin", 3);
    person1.displayInfo();
    Person person2("ReshiPriya", 8);
    person2.displayInfo();

    return 0;
}