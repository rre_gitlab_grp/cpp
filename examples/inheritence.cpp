#include <iostream>
using namespace std;
class Animal{
private:
public:
    void eat()
    {
        cout << "Animal is eating" << endl;
    }

};

class Dog : public Animal{
private:
public:
    void bark()
    {
        cout << "Dog is barking" << endl;

    }
};

int main()
{
    Dog myDog;
    myDog.bark();
    myDog.eat();
    return 0;
}