
// UserExample.cpp

#include "UserService.h"
#include "UserServiceImpl.cpp"  // Include the implementation files for services
#include "ProductServiceImpl.cpp"
#include "OrderServiceImpl.cpp"
#include "NotificationServiceImpl.cpp"

int main() {
    UserServiceImpl userService;
    ProductServiceImpl productService;
    OrderServiceImpl orderService(userService, productService);
    NotificationServiceImpl notificationService(userService);

    // Example: Place an order and send a notification.
    int userId = 123;
    int productId = 456;
    int quantity = 2;

    std::string orderResult = orderService.placeOrder(userId, productId, quantity);
    notificationService.sendNotification(userId, "Your order has been placed.");

    return 0;
}
