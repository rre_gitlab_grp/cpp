// NotificationService.h
#pragma once
#include <string>

class NotificationService {
public:
    virtual void sendNotification(int userId, const std::string& message) = 0;
    // Other notification-related methods...
};
