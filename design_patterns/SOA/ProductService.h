// ProductService.h
#pragma once
#include <string>

class ProductService {
public:
    virtual std::string getProductInfo(int productId) = 0;
    // Other product-related methods...
};
