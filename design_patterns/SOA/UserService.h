// UserService.h
#pragma once
#include <string>

class UserService {
public:
    virtual std::string getUserInfo(int userId) = 0;
    // Other user-related methods...
};
