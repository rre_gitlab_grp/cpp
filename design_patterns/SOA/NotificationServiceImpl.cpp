// NotificationServiceImpl.cpp
#include "NotificationService.h"
#include "UserService.h"

class NotificationServiceImpl : public NotificationService {
private:
    UserService& userService;

public:
    NotificationServiceImpl(UserService& userService) : userService(userService) {}

    void sendNotification(int userId, const std::string& message) override {
        // Use UserService to get user information.
        std::string userInfo = userService.getUserInfo(userId);

        // Perform notification sending logic.
        std::cout << "Notification sent to user " << userId << ": " << message << std::endl;
    }
    // Other notification-related method implementations...
};
