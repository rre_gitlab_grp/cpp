### 1. **Singleton Pattern:**

- **Intent:** Ensure a class has only one instance and provide a global point of access to it.

- Example:

  ```c++
  class Singleton {
  private:
      Singleton() {}  // Private constructor to prevent direct instantiation
  public:
      static Singleton& getInstance() {
          static Singleton instance;
          return instance;
      }
      // Additional methods and data members can be added here
  };
  
  int main() {
      Singleton& mySingleton = Singleton::getInstance();
      // Use mySingleton
  
      return 0;
  }
  ```

### 2. **Factory Method Pattern:**

- **Intent:** Define an interface for creating an object, but let subclasses alter the type of objects that will be created.

- Example:

  ```c++
  class Product {
  public:
      virtual void display() = 0;
  };
  
  class ConcreteProductA : public Product {
  public:
      void display() override {
          cout << "Product A" << endl;
      }
  };
  
  class ConcreteProductB : public Product {
  public:
      void display() override {
          cout << "Product B" << endl;
      }
  };
  
  class Creator {
  public:
      virtual Product* createProduct() = 0;
  };
  
  class ConcreteCreatorA : public Creator {
  public:
      Product* createProduct() override {
          return new ConcreteProductA();
      }
  };
  
  class ConcreteCreatorB : public Creator {
  public:
      Product* createProduct() override {
          return new ConcreteProductB();
      }
  };
  
  int main() {
      Creator* creatorA = new ConcreteCreatorA();
      Product* productA = creatorA->createProduct();
      productA->display();
  
      Creator* creatorB = new ConcreteCreatorB();
      Product* productB = creatorB->createProduct();
      productB->display();
  
      return 0;
  }
  ```

### 3. **Observer Pattern:**

- **Intent:** Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

- Example:

  ```c++
  #include <iostream>
  #include <vector>
  
  class Observer {
  public:
      virtual void update(const std::string& message) = 0;
  };
  
  class ConcreteObserver : public Observer {
  private:
      std::string name;
  public:
      ConcreteObserver(const std::string& n) : name(n) {}
      void update(const std::string& message) override {
          std::cout << name << " received message: " << message << std::endl;
      }
  };
  
  class Subject {
  private:
      std::vector<Observer*> observers;
  public:
      void addObserver(Observer* observer) {
          observers.push_back(observer);
      }
  
      void notify(const std::string& message) {
          for (Observer* observer : observers) {
              observer->update(message);
          }
      }
  };
  
  int main() {
      ConcreteObserver observerA("Observer A");
      ConcreteObserver observerB("Observer B");
  
      Subject subject;
      subject.addObserver(&observerA);
      subject.addObserver(&observerB);
  
      subject.notify("Important event!");
  
      return 0;
  }
  ```

### 4. **Strategy Pattern:**

- **Intent:** Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

- Example:

  ```c++
  class Strategy {
  public:
      virtual void execute() = 0;
  };
  
  class ConcreteStrategyA : public Strategy {
  public:
      void execute() override {
          cout << "Executing Strategy A" << endl;
      }
  };
  
  class ConcreteStrategyB : public Strategy {
  public:
      void execute() override {
          cout << "Executing Strategy B" << endl;
      }
  };
  
  class Context {
  private:
      Strategy* strategy;
  public:
      Context(Strategy* s) : strategy(s) {}
  
      void setStrategy(Strategy* s) {
          strategy = s;
      }
  
      void executeStrategy() {
          strategy->execute();
      }
  };
  
  int main() {
      ConcreteStrategyA strategyA;
      ConcreteStrategyB strategyB;
  
      Context context(&strategyA);
      context.executeStrategy();
  
      context.setStrategy(&strategyB);
      context.executeStrategy();
  
      return 0;
  }
  ```

### 5. **Decorator Pattern:**

- **Intent:** Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

- Example:

  ```c++
  class Component {
  public:
      virtual void operation() = 0;
  };
  
  class ConcreteComponent : public Component {
  public:
      void operation() override {
          cout << "ConcreteComponent operation" << endl;
      }
  };
  
  class Decorator : public Component {
  private:
      Component* component;
  public:
      Decorator(Component* c) : component(c) {}
      void operation() override {
          component->operation();
      }
  };
  
  class ConcreteDecoratorA : public Decorator {
  public:
      ConcreteDecoratorA(Component* c) : Decorator(c) {}
      void operation() override {
          Decorator::operation();
          cout << "ConcreteDecoratorA operation" << endl;
      }
  };
  
  class ConcreteDecoratorB : public Decorator {
  public:
      ConcreteDecoratorB(Component* c) : Decorator(c) {}
      void operation() override {
          Decorator::operation();
          cout << "ConcreteDecoratorB operation" << endl;
      }
  };
  
  int main() {
      ConcreteComponent concreteComponent;
      ConcreteDecoratorA decoratorA(&concreteComponent);
      ConcreteDecoratorB decoratorB(&decoratorA);
  
      decoratorB.operation();
  
      return 0;
  }
  ```



### 6. **Adapter Pattern:**

- **Intent:** Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

- Example:

  ```c++
  // Existing class with an incompatible interface
  class Adaptee {
  public:
      void specificRequest() {
          cout << "Adaptee's specific request" << endl;
      }
  };
  
  // Target interface expected by the client
  class Target {
  public:
      virtual void request() = 0;
  };
  
  // Adapter that adapts Adaptee to the Target interface
  class Adapter : public Target {
  private:
      Adaptee* adaptee;
  public:
      Adapter(Adaptee* a) : adaptee(a) {}
      void request() override {
          adaptee->specificRequest();
      }
  };
  
  int main() {
      Adaptee adaptee;
      Adapter adapter(&adaptee);
  
      adapter.request();
  
      return 0;
  }
  ```

### 7. **Command Pattern:**

- **Intent:** Encapsulate a request as an object, thereby parameterizing clients with different requests, queuing, and logging of requests, and providing undoable operations.

- Example:

  ```c++
  // Command interface
  class Command {
  public:
      virtual void execute() = 0;
  };
  
  // ConcreteCommand class
  class ConcreteCommand : public Command {
  private:
      Receiver* receiver;
  public:
      ConcreteCommand(Receiver* r) : receiver(r) {}
      void execute() override {
          receiver->action();
      }
  };
  
  // Receiver class
  class Receiver {
  public:
      void action() {
          cout << "Receiver's action" << endl;
      }
  };
  
  // Invoker class
  class Invoker {
  private:
      Command* command;
  public:
      void setCommand(Command* c) {
          command = c;
      }
      void executeCommand() {
          command->execute();
      }
  };
  
  int main() {
      Receiver receiver;
      ConcreteCommand concreteCommand(&receiver);
      Invoker invoker;
  
      invoker.setCommand(&concreteCommand);
      invoker.executeCommand();
  
      return 0;
  }
  ```

### 8. **Template Method Pattern:**

- **Intent:** Define the skeleton of an algorithm in the superclass but lets subclasses alter specific steps of the algorithm without changing its structure.

- Example:

  ```c++
  // Abstract class with a template method
  class AbstractClass {
  public:
      // Template method
      void templateMethod() {
          step1();
          step2();
          step3();
      }
  private:
      virtual void step1() = 0;
      virtual void step2() = 0;
      virtual void step3() = 0;
  };
  
  // ConcreteClassA and ConcreteClassB provide specific implementations
  class ConcreteClassA : public AbstractClass {
  private:
      void step1() override {
          cout << "ConcreteClassA - Step 1" << endl;
      }
      void step2() override {
          cout << "ConcreteClassA - Step 2" << endl;
      }
      void step3() override {
          cout << "ConcreteClassA - Step 3" << endl;
      }
  };
  
  class ConcreteClassB : public AbstractClass {
  private:
      void step1() override {
          cout << "ConcreteClassB - Step 1" << endl;
      }
      void step2() override {
          cout << "ConcreteClassB - Step 2" << endl;
      }
      void step3() override {
          cout << "ConcreteClassB - Step 3" << endl;
      }
  };
  
  int main() {
      ConcreteClassA concreteA;
      ConcreteClassB concreteB;
  
      concreteA.templateMethod();
      concreteB.templateMethod();
  
      return 0;
  }
  ```

These additional examples cover a range of design patterns that address various software design challenges. Understanding and applying these patterns appropriately can significantly enhance the modularity, flexibility, and maintainability of your code. Feel free to ask if you have any specific questions about these patterns or if you'd like more examples!